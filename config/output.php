<?php 
    include "conn.php";
    $limit_per_page = 5;

    $page = 0;
    $data = "";
    if (isset($_POST["page_no"])) {
        $page = $_POST["page_no"];
    } else {
        $page = 1;
    }

    $offset = ((int)$page - 1) * $limit_per_page;

    $sql = "SELECT*FROM user_info LIMIT {$offset},{$limit_per_page}";
    $q = $db->query($sql);
    $q->execute();
    $result = $q->fetchAll();
    $total_row = $q->rowCount();
      
    if ($total_row > 0) {
        $data .= '<table id="user_table" class="table table-bordered table-stiped">
                    <thead class="thead-dark">
                        <tr>
                            <th>STT</th>
                            <th>First Name</th>
                            <th>Mid Name</th>
                            <th>Last Name</th>
                            <th>Birthday</th>
                            <th>Sex</th>
                            <th>Addrres</th>
                            <th>Email</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        <tr>
                    </thead>';
        
        foreach($result as $row) {
            $offset++;
            $data .= '<tr>
                        <td>' .$offset. '</td> 
                        <td style="text-transform: capitalize;">' .($row['ho']).       '</td>
                        <td style="text-transform: capitalize;">' .($row['tendem']).   '</td>
                        <td style="text-transform: capitalize;">' .($row['ten']).      '</td>
                        <td>' .($row['ngay_sinh']).'</td>
                        <td>' .($row['gioi_tinh']).'</td>
                        <td>' .($row['dia_chi']).  '</td>
                        <td>' .($row['email']).    '</td>
                        <td><button class="btn btn-danger" id="delete" onclick="deleteUser('.$row['id'].')">Delete</button> </td>
                        <td><button class="btn btn-warning" name="edit" onclick="getUserDetails('.$row['id'].')">Edit</a></button></td>
                    </tr>';
        }
        $data .= '</table>';

        $sql_total = "SELECT * From user_info";
        $record = $db->query($sql_total);
        $total_record = $record->rowCount();
        $total_page = ceil($total_record/$limit_per_page);

        $data .= "<div id='pagination'>
                    <nav aria-label='Page navigation example'>
                        <ul class='pagination justify-content-end'>";

        if ($page > 1) {
            $previous = $page - 1;
            $data .= "<a class='page-item' id='1'><span class='page-link'>First Page</span></a>";
            $data .= '<a class="page-item" id="' . $previous . '"><span class="page-link"><i class="fa fa-arrow-left"></i></span></a>';
        }
        for ($i = 1; $i <= $total_page; $i++) {
            $active_class = "";
            if ($i == $page
            ) {
                $active_class = "active";
            }

            $data .='<a class="page-item ' . $active_class . '" id="' . $i . '"><span class="page-link">' . $i . '</span></a>';
        }


        if ($page < $total_page) {
            $page++;
            $data .= '<a class="page-item" id="' . $page . '"><span class="page-link"><i class="fa fa-arrow-right"></i></span></a>';
            $data .= '<a class="page-item" id="' . $total_page . '"><span class="page-link">Last Page</span></a>';
        }
        $data .= '</ul>';

        $data .=   "</ul>
                </nav>";

        echo $data;
    } else {
        $data .=
            '<tr> 
                <td colspan="4" align="center">Data not found</td>
            </tr>';
    }
?>