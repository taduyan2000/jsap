<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/crud.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>CRUD</title>
</head>

<body>
    <div class="container">
        <h1 class="text-primary text-uppercase text-center">CRUD</h1>

        <div class="search">
            <div class="input-group">
                <form action="" method="post" style="display: flex;">
                    <input type="text" class="form-control" name="search_text" style=" border-radius: 15px 0px 0px 15px; padding: 5px;" id="search_text" placeholder="Tìm kiếm trên trang web">
                    <button type="button" class="btn btn-success" style="border-radius: 0px 15px 15px 0px;" onclick="search()">Tìm</button>
                </form>
            </div>

            <div id="result"></div>
            <p id="tb"></p>
        </div>

        <div class="d-flex justify-content-end">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add User</button>
        </div>

        <div>
            <h2 class="text-danger">User Infomation</h2>
            <div id="records_contant">
            </div>
        </div>

        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Add User Infomation</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form method="post" name="input_data" id="user_form">
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" name="id" id="id" class="form-control" style="display: none">
                            </div>

                            <div class="form-group">
                                <label> Firstname: </label>
                                <input type="text" name="firstname" id="firstname" required="required" class="form-control" placeholder="First Name">
                                <span id="error_first_name" class="text-danger"></span>
                            </div>

                            <div class="form-group">
                                <label> Midname: </label>
                                <input type="text" name="midname" id="midname" class="form-control" required="required" placeholder="Mid Name">
                                <span id="error_mid_name" class="text-danger"></span>
                            </div>

                            <div class="form-group">
                                <label> Lastname: </label>
                                <input type="text" name="lastname" id="lastname" class="form-control" required="required" placeholder="Last Name">
                                <span id="error_last_name" class="text-danger"></span>
                            </div>

                            <div class="form-group">
                                <label for="birthday">Birthday</label><br>
                                <input type="date" name="birtday" id="birthday" class="form-control" required="required" placeholder="Birthday" value="">
                                <span id="error_birthday" class="text-danger"></span>
                            </div>

                            <div class="form-group">
                                <label for="sex">Sex</label>
                                <input type="radio" name="sex" value="Nam" id="sex" style="margin-left: 2rem;" checked="checked">
                                <label for="male">Nam</label>
                                <input type="radio" name="sex" value="Nữ" id="sex">
                                <label for="female">Nữ</label>
                            </div>

                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" name="address" id="address" value="" required="required" class="form-control" placeholder="Address">
                                <span id="error_address" class="text-danger"></span>
                            </div>

                            <div class="form-group">
                                <label>Email</label><br>
                                <input type="text" name="email" id="email" value="" required="required" class="form-control" placeholder="email">
                                <span id="emailMsg" style="color:rgb(220,53,69)"></span>
                            </div>
                        </div>
                    </form>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" id="scrollelement" class="btn btn-success" onclick="addRecord()">Add</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>

            </div>

        </div>

        <!--model update-->
        <div class="modal" id="update_user_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" id="update_user_form">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Update Info</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>


                        <!-- Modal body -->
                        <div class="modal-body">

                            <div class="form-group">
                                <label> Firstname: </label>
                                <input type="text" name="update_firstname" id="update_firstname" required="required" class="form-control" placeholder="First Name">
                                <span id="error_first_name" class="text-danger"></span>
                            </div>

                            <div class="form-group">
                                <label> Midname: </label>
                                <input type="text" name="update_midname" id="update_midname" required="required" class="form-control" placeholder="Mid Name">
                                <span id="error_mid_name" class="text-danger"></span>
                            </div>

                            <div class="form-group">
                                <label> Lastname: </label>
                                <input type="text" name="update_lastname" id="update_lastname" required="required" class="form-control" placeholder="Last Name">
                                <span id="error_last_name" class="text-danger"></span>
                            </div>

                            <div class="form-group">
                                <label for="birthday">Birthday</label><br>
                                <input type="date" name="update_birtday" id="update_birthday" required="required" class="form-control" placeholder="Birthday" value=""><br>
                                <span id="error_birthday" class="text-danger"></span>
                            </div>

                            <div class="form-group">
                                <label for="sex">Sex</label>
                                <input type="radio" name="sex_update" class="sex_update" value="Nam" id="male" style="margin-left: 2rem;" checked="checked">
                                <label for="male">Nam</label>
                                <input type="radio" name="sex_update" class="sex_update" value="Nữ" id="female">
                                <label for="female">Nữ</label>
                            </div>

                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" name="update_address" id="update_address" value="" required="required" class="form-control" placeholder="Address">
                                <span id="error_address" class="text-danger"></span>
                            </div>

                            <div class="form-group">
                                <label>Email</label><br>
                                <input type="text" name="update_email" id="update_email" value="" required="required" class="form-control" placeholder="email">
                            </div>
                        </div>
                    </form>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" name="form_action" id="form_action" class="btn btn-success" onclick="Updateuserdetails()">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <input type="hidden" name="" id="hidden_user_id">
                    </div>

                </div>

            </div>

        </div>

    </div>

    <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.js" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="crud.js"></script>
</body>

</html>