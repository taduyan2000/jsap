// readRecord();

// function readRecord() {
//     $.ajax({
//         url: "http://localhost:8080/taan/jsap/config/output.php",
//         method: "POST",
//         success: function(data, status) {
//             $('#records_contant').html(data);
//         }
//     });
// }

function loadTable(page) {
    $.ajax({
        url: "http://localhost:8080/taan/jsap/config/output.php",
        type: "POST",
        data: { page_no: page },
        success: function(data) {
            $('#records_contant').html(data);
        }
    });
}
loadTable();

$(document).on("click", "#pagination a", function(e) {
    e.preventDefault();
    var page_id = $(this).attr("id");
    loadTable(page_id);
});

function paginate(page_id) {
    loadTable(page_id);
}

$('#search_text').keyup(function() {
    var txt = $(this).val();
    if (txt != '') {
        $.ajax({
            url: "http://localhost:8080/taan/jsap/config/search.php",
            type: "post",
            data: {
                search: txt
            },
            dataType: "text",
            success: function(data, status) {
                $('#result').html(data);
            }
        });
    } else {
        $('#result').html('');
    }
});



$().ready(function() {
    $('#user_form').validate({
        rules: {
            firstname: "required",
            midname: "required",
            lastname: "required",
            birthday: "required",
            address: "required",
            email: "required"
        },
        messages: {
            firstname: "please enter your first name",
            midname: "please enter your mid name",
            lastname: "please enter your last name",
            birthday: "please enter your birthday",
            address: "please enter your address",
            email: "please enter your email"
        }
    });
});

function addRecord() {
    if ($("#user_form").valid()) {
        $("#myModal").modal("hide");

        $("html, body").animate({
            scrollTop: $(
                'html, body').get(0).scrollHeight
        }, 1000);

        if (validateEmail()) {
            //cuộn xuống sau khi xử lí
            var firstname = $('#firstname').val();
            var midname = $('#midname').val();
            var lastname = $('#lastname').val();
            var birthday = $('#birthday').val();
            var address = $('#address').val();
            var sex = document.querySelector('input[name = "sex"]:checked').value;
            var email = $('#email').val();

            $.ajax({
                url: "http://localhost:8080/taan/jsap/config/backend1.php",
                type: "POST",
                data: {
                    firstname: firstname,
                    midname: midname,
                    lastname: lastname,
                    birthday: birthday,
                    address: address,
                    sex: sex,
                    email: email
                },
                success: function(data, status) {
                    // readRecord();
                    loadTable();
                    document.forms['input_data'].reset();
                    swal("Thêm Thành Công!", "Dữ liệu đã được làm mới!", "success", {
                        buttons: false,
                        timer: 2000,
                    });
                }
            });
        }
    }
}


function deleteUser(deleteid) {
    swal({
            title: "Bạn có chắc muốn xóa user này?",
            text: "",
            icon: "warning",
            buttons: ["Close", "Delete"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: "http://localhost:8080/taan/jsap/config/delete.php",
                    type: "post",
                    data: {
                        deleteid: deleteid
                    },
                    success: function(data, status) {
                        // readRecord();
                        // paginate(page_id);
                        loadTable();

                    }
                });
                swal("Xóa Thành Công", {
                    icon: "success",
                    buttons: false,
                    timer: 3000,
                });
            }
        });
}

function getUserDetails(id) {
    $('#hidden_user_id').val(id);
    $.post("http://localhost:8080/taan/jsap/config/edit.php", {
            id: id
        }, function(data, status) {
            var user = JSON.parse(data);
            $('#update_firstname').val(user.ho);
            $('#update_midname').val(user.tendem);
            $('#update_lastname').val(user.ten);
            $('#update_birthday').val(user.ngay_sinh);
            $('#sex').val(user.gioi_tinh);
            $('#update_address').val(user.dia_chi);
            $('#update_email').val(user.email);
        }

    );
    $('#update_user_modal').modal("show");
}


$().ready(function() {
    $('#update_user_form').validate({
        rules: {
            update_firstname: "required",
            update_midname: "required",
            update_lastname: "required",
            update_birthday: "required",
            update_address: "required",
            update_email: "required"
        },
        messages: {
            update_firstname: "please enter your first name",
            update_midname: "please enter your mid name",
            update_lastname: "please enter your last name",
            update_birthday: "please enter your birthday",
            update_address: "please enter your address",
            update_email: "please enter your email"
        }
    });
});

function Updateuserdetails() {
    if ($('#update_user_form').valid()) {
        var firstname = $('#update_firstname').val();
        var midname = $('#update_midname').val();
        var lastname = $('#update_lastname').val();
        var birthday = $('#update_birthday').val();
        var sex = document.querySelector('input[name = "sex_update"]:checked').value;
        var address = $('#update_address').val();
        var email = $('#update_email').val();
        var hidden_user_id = $('#hidden_user_id').val();

        $.post("http://localhost:8080/taan/jsap/config/edit.php", {
                hidden_user_id: hidden_user_id,
                firstname: firstname,
                midname: midname,
                lastname: lastname,
                birthday: birthday,
                sex: sex,
                address: address,
                email: email,
            },
            function(data, status) {
                $('#update_user_modal').modal("hide");
                // readRecord();
                loadTable();
                swal("Sửa thông tin thành công!", "Dữ liệu đã được làm mới!", "success", {
                    buttons: false,
                    timer: 2000,
                });
            }
        );
    } else {
        if ($('#update_firstname').valid() != true) {
            $("#update_firstname").css("border", "3px solid red");
            $("#btn").show();
        }
        if ($('#update_midname').valid() != true) {
            $("#update_midname").css("border", "3px solid red");
        }
        if ($('#update_lastname').valid() != true) {
            $("#update_lastname").css("border", "3px solid red");
        }
        if ($('#update_birthday').valid() != true) {
            $("#update_birthday").css("border", "3px solid red");
        }
        if ($('#update_address').valid() != true) {
            $("#update_address").css("border", "3px solid red");
        }
        if ($('#update_email').valid() != true) {
            $("#update_email").css("border", "3px solid red");
        }

        swal("Bạn chưa nhập đủ thông tin!", {
            buttons: false,
            timer: 2000,
        });
        $('#update_user_modal').modal("show");
    }
}

$(document).ready(function() {
    // set initially button state hidden
    $("#btn").hide();
    // use keyup event on email field
    $("#email").keyup(function() {
        if (validateEmail()) {
            // if the email is validated
            // set input email border green
            $("#email").css("border", "3px solid green");
            // and set label 
            $("#emailMsg").html("<p class='text-success'>Email Khả dụng</p>");
        } else {
            // if the email is not validated
            // set border red
            $("#email").css("border", "3px solid red");
            $("#emailMsg").html("<p class='text-danger'>Email Không khả dụng</p>");
        }
        buttonState();
    });
});

function buttonState() {
    if (validateEmail()) {
        // if the both email and password are validate
        // then button should show visible
        $("#btn").show();
    } else {
        // if both email and pasword are not validated
        // button state should hidden
        $("#btn").hide();
    }
}

function validateEmail() {
    // get value of input email
    var email = $("#email").val();
    // use reular expression
    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if (reg.test(email)) {
        return true;
    } else {
        return false;
    }

}

//UpTop
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

// $(document).ready(function() {
//     function loadTable(page) {
//         $.ajax({
//             url: "http://localhost:8080/taan/jsap/config/output.php",
//             type: "POST",
//             data: { page_no: page },
//             success: function(data) {
//                 $('#user_table').html(data);
//             }
//         });
//     }
//     loadTable();

//     $(document).on("click", "#pagination a", function(e) {
//         e.preventDefault();
//         var page_id = $(this).attr("id");

//         loadTable(page_id);
//     });

//     function paginate(page_id) {
//         loadTable(page_id);
//     }
// });