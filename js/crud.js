readRecord();

function readRecord() {
    $.ajax({
        url: "http://localhost:8080/taan/jsap/config/output.php",
        method: "POST",
        success: function(data, status) {
            $('#records_contant').html(data);
        }
    });
}

function addRecord() {
    event.preventDefault();
    var error_first_name = '';
    var error_mid_name = '';
    var error_last_name = '';
    var error_birthday = '';
    var error_sex = '';
    var error_address = '';
    var error_email = '';

    if ($('#birthday').val() == '' && $('#firstname').val() == '' && $('#address').val() == '' &&
        $('#midname').val() == '' && $('#lastname').val() == '' && $('#email').val() == '') {
        error_birthday = 'Birthday is required';
        $('#error_birthday').text(error_birthday);
        $("#birthday").css("border", "3px solid red");
        error_first_name = 'First name is required';
        $('#error_first_name').text(error_first_name);
        $("#firstname").css("border", "3px solid red");
        error_mid_name = 'Mid name is required';
        $('#error_mid_name').text(error_mid_name);
        $("#midname").css("border", "3px solid red");
        error_last_name = 'Last name is required';
        $('#error_last_name').text(error_last_name);
        $("#lastname").css("border", "3px solid red");
        error_address = 'Address is required';
        $('#error_address').text(error_address);
        $('#address').css("border", "3px solid red");
        error_sex = 'sex is required';
        $('#error_sex').text(error_sex);
        error_email = 'Email is required';
        $('#emailMsg').text(error_email);
        $("#email").css("border", "3px solid red");

        $("#myModal").modal("show");

        swal("Thông tin chưa được nhập đủ!", {
            buttons: false,
            timer: 2000,
        });

    } else if ($('#address').val() == '') {
        error_address = 'Address is required';
        $('#error_address').text(error_address);
        $('#address').css('border-color:red', '');

        $("#myModal").modal("show");

        swal("Thông tin chưa được nhập đủ!", {
            buttons: false,
            timer: 2000,
        });
    } else if ($('#birthday').val() == '') {
        error_birthday = 'Birthday is required';
        $('#error_birthday').text(error_birthday);
        $('#birthday').css('border-color:red', '');

        $("#myModal").modal("show");

        swal("Thông tin chưa được nhập đủ!", {
            buttons: false,
            timer: 2000,
        });
    } else if ($('#lastname').val() == '') {
        error_last_name = 'Last name is required';
        $('#error_last_name').text(error_last_name);
        $('#lastname').css('border-color:red', '');

        $("#myModal").modal("show");

        swal("Thông tin chưa được nhập đủ!", {
            buttons: false,
            timer: 2000,
        });
    } else if ($('#midname').val() == '') {
        error_mid_name = 'Mid name is required';
        $('#error_mid_name').text(error_mid_name);
        $('#midname').css('border-color:red', '');

        $("#myModal").modal("show");

        swal("Thông tin chưa được nhập đủ!", {
            buttons: false,
            timer: 2000,
        });
    } else if ($('#firstname').val() == '') {
        error_first_name = 'First name is required';
        $('#error_first_name').text(error_first_name);
        $('#firstname').css('border-color:red', '');

        $("#myModal").modal("show");

        swal("Thông tin chưa được nhập đủ!", {
            buttons: false,
            timer: 2000,
        });
    } else if ($('#email').val() == '') {
        error_email = 'Email is required';
        $('#emailMsg').text(error_email);
        $('#email').css('border-color:red', '');

        $("#myModal").modal("show");

        swal("Thông tin chưa được nhập đủ!", {
            buttons: false,
            timer: 2000,
        });
    }

    if ($('#birthday').val() != '' && $('#firstname').val() != '' && $('#address').val() != '' &&
        $('#midname').val() != '' && $('#lastname').val() != '' && $('#email').val() != '') {

        $("#myModal").modal("hide");

        $("html, body").animate({
            scrollTop: $(
                'html, body').get(0).scrollHeight
        }, 1000);

        if (validateEmail()) {
            //cuộn xuống sau khi xử lí
            var firstname = $('#firstname').val();
            var midname = $('#midname').val();
            var lastname = $('#lastname').val();
            var birthday = $('#birthday').val();
            var address = $('#address').val();
            var sex = document.querySelector('input[name = "sex"]:checked').value;
            var email = $('#email').val();

            $.ajax({
                url: "http://localhost:8080/taan/jsap/config/backend1.php",
                type: "POST",
                data: {
                    firstname: firstname,
                    midname: midname,
                    lastname: lastname,
                    birthday: birthday,
                    address: address,
                    sex: sex,
                    email: email
                },
                success: function(data, status) {
                    readRecord();
                    document.forms['input_data'].reset();
                    swal("Thêm Thành Công!", "Dữ liệu đã được làm mới!", "success", {
                        buttons: false,
                        timer: 2000,
                    });
                }
            });
        }
    }
}

function deleteUser(deleteid) {
    swal({
            title: "Bạn có chắc muốn xóa user này?",
            text: "",
            icon: "warning",
            buttons: ["Close", "Delete"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: "http://localhost:8080/taan/jsap/config/delete.php",
                    type: "post",
                    data: {
                        deleteid: deleteid
                    },
                    success: function(data, status) {
                        readRecord();
                    }
                });
                swal("Xóa Thành Công", {
                    icon: "success",
                    buttons: false,
                    timer: 3000,
                });
            }
        });
}

$(function() {
    var $egistrerForm = $('#user_form');
    if ($registerForm.length) {
        $registerForm.validate({
            rules: {
                firstname: {
                    required: true
                }
                // midname
                // lastname
                // birthday
                // sex
                // address
                // email
            },
            messages: {
                firstname: {
                    required: "Bạn chưa điền họ"
                }
            }
        })
    }
});

function getUserDetails(id) {
    $('#hidden_user_id').val(id);
    $.post("http://localhost:8080/taan/jsap/config/edit.php", {
            id: id
        }, function(data, status) {
            var user = JSON.parse(data);
            $('#update_firstname').val(user.ho);
            $('#update_midname').val(user.tendem);
            $('#update_lastname').val(user.ten);
            $('#update_birthday').val(user.ngay_sinh);
            $('#sex').val(user.ngay_sinh);
            $('#update_address').val(user.dia_chi);
            $('#update_email').val(user.email);
        }

    );
    $('#update_user_modal').modal("show");
}

function Updateuserdetails() {
    var firstname = $('#update_firstname').val();
    var midname = $('#update_midname').val();
    var lastname = $('#update_lastname').val();
    var birthday = $('#update_birthday').val();
    var sex = document.querySelector('input[name = "sex_update"]:checked').value;
    var address = $('#update_address').val();
    var email = $('#update_email').val();
    var hidden_user_id = $('#hidden_user_id').val();

    $.post("http://localhost:8080/taan/jsap/config/edit.php", {
            hidden_user_id: hidden_user_id,
            firstname: firstname,
            midname: midname,
            lastname: lastname,
            birthday: birthday,
            sex: sex,
            address: address,
            email: email,
        },
        function(data, status) {
            $('#update_user_modal').modal("hide");
            readRecord();
            swal("Sửa thông tin thành công!", "Dữ liệu đã được làm mới!", "success", {
                buttons: false,
                timer: 2000,
            });
        }
    );
}

$(document).ready(function() {
    // set initially button state hidden
    $("#btn").hide();
    // use keyup event on email field
    $("#email").keyup(function() {
        if (validateEmail()) {
            // if the email is validated
            // set input email border green
            $("#email").css("border", "3px solid green");
            // and set label 
            $("#emailMsg").html("<p class='text-success'>Email Khả dụng</p>");
        } else {
            // if the email is not validated
            // set border red
            $("#email").css("border", "3px solid red");
            $("#emailMsg").html("<p class='text-danger'>Email Không khả dụng</p>");
        }
        buttonState();
    });
});

function buttonState() {
    if (validateEmail()) {
        // if the both email and password are validate
        // then button should show visible
        $("#btn").show();
    } else {
        // if both email and pasword are not validated
        // button state should hidden
        $("#btn").hide();
    }
}

function validateEmail() {
    // get value of input email
    var email = $("#email").val();
    // use reular expression
    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if (reg.test(email)) {
        return true;
    } else {
        return false;
    }

}

//UpTop
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
//  document.documentElement.scrollTop = 0;
//